# PowerJDE

A [powerline][] + [Solarized][] prompt for [zsh][].

## Requirements

* This prompt system assumes that you are using a font like [PragmataPro][],
  which contains certain customized glyphs (so-called *powerline* glyphs, in
  reference to the [powerline][] project which originated the style). These
  glyphs, used sparingly and in the right places, make for a bad ass looking
  prompt.

* The prompt shows certain special information when your working directory is
  in a git repository, and so assumes you have [git][] installed. An included
  helper utility to obtain git information requires [python][] (either version
  2.x or 3.x should work).

* The project was developed and tested under zsh-5.0.5 on Linux, but
  a predecessor version was tested under various operating systems including
  MacOS, FreeBSD, OpenBSD, and Cygwin. It should be made to work pretty easily
  anywhere you can run zsh.

* Basically, the prompt requires a color terminal. Three terminal color modes
  are supported:

  - **16 colors**. This legacy mode has been supported in terminals since the
    1980's. To use this mode with [Solarized][], you will need to configure
    your terminal's 16 color palette to use the special Solarized colors. This
    is a small investment in effort but, according  to the Solarized creators,
    is well worth it since it will produce the best looking results.

  - **256 colors**. This is easy to configure with a 256 color terminal,
    requiring no special configuration of your terminal software, other than
    possibly getting your `terminfo` database correct (see below for more about
    that). The colors used in this mode are only approximations to the
    specially crafted optimum colors chosen by the Solarized developers, so the
    results are not quite as good as the other options. Use your own judgement,
    but read up on the [Solarized][] website why it may be worth it to you to
    put the effort in to obtaining the best possible color.

  - **16.7 million colors**. A feature started by the [konsole][] project, but
    is now quickly spreading to other terminal emulator projects, this approach
    is the best of all worlds, since it allows precise color specification
    without additional terminal configuration. If you use [konsole][] or one of
    the other terminals which support this color mode, then this approach is
    recommended.

## Installation

Obtain the code:

    git clone https://jdelkins@bitbucket.org/jdelkins/zsh-prompt-powerjde.git

and then, add to your `~/.zshrc` something like the following:

    fpath+=/path/to/zsh-prompt-powerjde
    promptinit
    prompt powerjde 256dark

Note that the `256dark` option indicates that you wish to use xterm 256 ANSI
colors in the [Solarized][] dark style. Note that the full list of color mode
and styles are as follows:

- `16dark`
- `16light`
- `256dark`
- `256light`
- `16Mdark`
- `16Mlight`

## Git

The accompanying [python][] script `gitstatus.py` is a helper to gather info
about the git repository your shell is sitting on, if you are on one. This
obviously uses and depends on [git][].

## Terminfo

The default xterm terminfo for some OS's may not recognize 256 color codes, or
may not work well for other reasons. There are a couple of ways to handle this.
The easiest is probably to set your `TERM` environment variable to a terminal
type that supports 256 colors. This can be done in the shell (`~/.zshrc` for
example) as follows:

    TERM=xterm-256color

Slightly better is to have your terminal software properly report this
capability. In [PuTTY][], for example, you can go to `Connection -> Data ->
Terminal-type string` and set that to `xterm-256color`. This is a good default.

A final alternative is to give your host an updated definition of the
capabilities of an `xterm` terminal. This can be done simply by using the
included `xterm.terminfo` file, as follows:

    tic xterm.terminfo

This procedure will create a user specific terminfo entry for the `xterm`
terminal in `~/.terminfo`, advertising 256 ANSI color modes and other niceties
that this prompt may benefit from.

## Bugs

If you find a bug, I thank you in advance for your kindly opening an issue or
pull request on [bitbucket][].

[powerline]:   https://github.com/powerline/powerline
[Solarized]:   http://ethanschoonover.com/solarized
[zsh]:         http://www.zsh.org/
[PragmataPro]: http://www.fsd.it/fonts/pragmatapro.htm#.Uv74Y_ldVgg
[git]:         http://git-scm.com/
[konsole]:     http://konsole.kde.org/
[PuTTY]:       http://www.chiark.greenend.org.uk/~sgtatham/putty/
[python]:      http://www.python.org/
[bitbucket]:   https://bitbucket.org/jdelkins/zsh-prompt-powerjde
